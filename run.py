from apps.routes import init_routes
from apps.database import *
from apps.app import app

api_run = '0.0.0.0'

# init_db()
init_routes()
app.run(host=api_run, port=5001)
