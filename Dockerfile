FROM ubuntu:18.04
# Update & Install dcmtk
#RUN apt-get update -y
#RUN cat /etc/apt/sources.list
#RUN apt-get install build-essential python3.6 python3.6-dev python3-pip python3.6-venv -y
RUN apt-get update
RUN apt-get install -y build-essential python3.6 python3.6-dev python3-pip python3.6-venv
RUN cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

# Prepare cxr program & data dir
#RUN mkdir -p /data/dicom2/dicom_image
RUN mkdir -p /data/ocr/image
RUN mkdir -p /data/ocr/pdf
#RUN mkdir -p /data/csv
RUN mkdir /ocr
WORKDIR /ocr

# Install python3 lib
#ADD requirement.txt .
#RUN pip3 install --no-cache-dir -r requirement.txt
#RUN apt-get install libmagickwand-dev

#


# Add python code to pwd
ADD . .
# Install imagemagick
RUN tar xzvf ImageMagick.tar.gz
RUN cd ImageMagick-7.0.8-68 && ./configure && make && make install && ldconfig /usr/local/lib

RUN apt-get install tesseract-ocr libtesseract-dev libleptonica-dev -y
RUN apt-get install libpng-dev libjpeg-dev libtiff-dev zlib1g-dev -y
#RUN apt-get install gcc g++
RUN apt-get install autoconf automake libtool checkinstall -y
#RUN wget http://www.leptonica.org/source/leptonica-1.73.tar.gz
#RUN tar xzvf leptonica-1.73.tar.gz
#RUN cd leptonica-1.73 && ./configure && make && checkinstall && ldconfig
#RUN apt-get install tesseract-ocr -y
RUN apt-get install tesseract-ocr-tha
RUN apt-get update -y

ADD requirement.txt .
RUN pip3 install --no-cache-dir -r requirement.txt
RUN apt-get install libmagickwand-dev -y

#COPY /home/sdiadmin/git/ocr-backend/Thai.traineddata /usr/share/tesseract-ocr/tessdata
#RUN ./configure 
#RUN make clean
#RUN make
#RUN checkinstall
#RUN ldconfig /usr/local/lib

CMD ["python", "run.py","--prod"]