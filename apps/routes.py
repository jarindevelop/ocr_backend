# -*- coding: utf-8 -*
print ('start init routing')
from .app import app
from .getimage.routing import routes as getimage_routes
from .getdetail.routing import routes as getdetail_routes
from .position.routing import routes as position_route
from .upload.routing import routes as upload_route

def root_endpoint():
    return 'OCR_BACKEND'

def init_routes():
    app.add_url_rule( '/',
                      'root_endpoint',
                      root_endpoint,
                      methods=['GET'],
                    )
    app.register_blueprint(position_route, url_prefix="/api/v1/position")
    app.register_blueprint(upload_route, url_prefix="/api/v1/upload")
    app.register_blueprint(getimage_routes, url_prefix="/api/v1/getimage")
    app.register_blueprint(getdetail_routes, url_prefix="/api/v1/getdetail")
print ('finish init routing')