# -*- coding: utf-8 -*
from apps.position import controller

print ('...start routing module position')
from flask import Blueprint

routes = Blueprint('/position',__name__)

routes.add_url_rule(
    '',
    'position',
    controller.getposition,
    methods = ['POST']
)
print ('...finish routing module position')