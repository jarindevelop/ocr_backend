# -*- coding: utf-8 -*
from pymongo import MongoClient
from .app import app
from flask_sqlalchemy import SQLAlchemy
import mysql.connector
import uuid
import json
import time
import sys


print("start init database..")


# def init_db():


mydb_name = 'ocr_backend'
host = 'localhost'
user = 'root'
password = ''

argv = sys.argv
if len(argv) > 1:
    if argv[1] == '--prod':
        host = 'db'
        user = 'sdiadmin'
        password = 'mis@Pass01'

mydb = mysql.connector.connect(host=host, user=user, password=password)
cur = mydb.cursor()
cur.execute("SHOW DATABASES")
db_list = []
for db_name in cur:
    # print("db_name :",db_name)
    db_list.append(db_name[0])
if mydb_name not in db_list:
    cur.execute("CREATE DATABASE " + mydb_name + " CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci")
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{0}:{1}@{2}/{3}?charset=utf8'.format(user, password, host,
                                                                                              mydb_name)
db = SQLAlchemy(app)

def generate_uuid():
    uid = uuid.uuid4()
    return str(uid)

class img_data(db.Model):
    __tablename__ = 'img_data'
    img_id = db.Column(db.String(100), primary_key=True, default=generate_uuid)
    img_name = db.Column(db.String(100))
    img_position = db.Column(db.Text())
