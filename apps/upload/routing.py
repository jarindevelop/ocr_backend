# -*- coding: utf-8 -*
from apps.upload import controller

print ('...start routing module upload')
from flask import Blueprint

routes = Blueprint('/upload',__name__)

routes.add_url_rule(
    '',
    'upload_file',
    controller.upload_file,
    methods = ['GET','POST']
)
print ('...finish routing module upload')